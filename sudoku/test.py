'''
Created on 2012-05-19

@author: mavc
'''

import unittest
from sudokusolve import SudokuSolver

class TestSudoku(unittest.TestCase):

    def setUp(self):
        self.s = SudokuSolver()

    def test_single_value(self):
        s = self.s

        for i in xrange(9):
            self.assertTrue(s._single_value(1 << i), 1 << i)

