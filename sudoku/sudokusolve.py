'''
Sudoku Solver.
'''

import itertools
import random

import numpy as np
from collections import deque


class SudokuSolver(object):
    '''
    Solves Sudoku as CSP using Arc Consistency + Domain Splitting.
    
    '''
    _ALL = (1 << 9) - 1
    _MASKS = [(1 << i) for i in xrange(9)]

    def __init__(self, seed=0):
        ''' Construct a SudokuSolver instance with the given seed. '''
        self._seed = seed
        self._r = random.Random(seed)

    def solve(self, board):
        self._r.seed(self._seed)
        board = np.array(board, dtype='intc')
        if board.shape != (9, 9):
            raise ValueError('Board must be 9 x 9')

        # Calculate domains for each of the 9x9 squares, coded as a bitmask
        # of possible values.
        nz = (board != 0)
        domains = np.zeros((9, 9), dtype='intc') + SudokuSolver._ALL
        domains[nz] = 1 << (board[nz] - 1)

        # Add constraint arcs from every position to its
        # boxrowcol positions.
        arcs = deque((a, b) for a in itertools.product(xrange(9), xrange(9))
                     for b in self._nb(a))
        solution = self._ac_ds(domains, arcs)

        if solution is not None:
            return self._board_from_domains(solution)
        else:
            raise ValueError('No solutions')

    def _arc_consistency(self, domains, arcs):
        while arcs:
            a, b = arcs.pop()
            domain_a, domain_b = domains[a], domains[b]

            # Make the domain of a consistent with the domain of b.
            # If b is a single value, then remove it from the domain of a.
            if self._single_value(domain_b):
                domain_a &= ~domain_b
                if domain_a == 0:
                    return False
                if domains[a] != domain_a:
                    domains[a] = domain_a
                    affected = {(nb, a) for nb in self._nb(a) if nb != b}
                    arcs.extend(affected)
        return True

    def _ac_ds(self, domains, arcs, check_solution=True):
        ''' Arc Consistency with Domain Splitting '''

        # Initialize domains with arc consistency.
        success = self._arc_consistency(domains, arcs)

        # If there are any empty domains, then there is no solution.
        if not success:
            return None

        # If all domains have a single value, then this is a solution.
        if np.all(self._single_value(domains)):
            return domains

        # Otherwise, recurse. Pick a variable to split on, by size of domain.
        split_idx = self._r.choice(self._split_idxs(domains))
        vals = [mask for mask in self._MASKS if domains[split_idx] & mask]

        affected = [(n, split_idx) for n in self._nb(split_idx)]
        solution = None

        for val in vals:
            newdomains = domains.copy()
            newdomains[split_idx] = val

            check = check_solution and solution is None
            result = self._ac_ds(newdomains, deque(affected), check)
            if result is not None:
                if not check_solution:
                    return result
                if solution is not None:
                    raise ValueError('More than one solution')
                solution = result

        #If we get here, then either 0 or 1 solutions found.
        return solution

    @staticmethod
    def _board_from_domains(domains):
        board = np.zeros((9, 9), dtype='intc')
        for i, val in zip(itertools.count(1), SudokuSolver._MASKS):
            board[domains == val] = i
        return board

    @staticmethod
    def _domain_sizes(domains):
        return sum((domains & mask) > 0 for mask in SudokuSolver._MASKS)

    @staticmethod
    def _split_idxs(domains):
        domainsizes = SudokuSolver._domain_sizes(domains)
        minsize = np.min(domainsizes[domainsizes > 1])
        return zip(*np.where(domainsizes == minsize))

    @staticmethod
    def _single_value(val):
        return (val & (val - 1) == 0)

    @staticmethod
    def _nb(n):
        '''
        Return a list of (x, y) tuples of the 20 positions in the same
        box, row, or column of n.
        '''
        x, y = n
        nb = (set(zip(xrange(9), itertools.repeat(y))) |
              set(zip(itertools.repeat(x), xrange(9))) |
              set(itertools.product(xrange(3 * (x // 3), 3 * (x // 3) + 3),
                                    xrange(3 * (y // 3), 3 * (y // 3) + 3))))
        nb.remove(n)
        return nb

def board_from_csv(csv):
    board = np.zeros((9, 9), dtype='intc')
    row = 0
    with open(csv, 'r') as f:
        for line in itertools.islice(f, 9):
            board[row] = map(int, line.rstrip().split(','))
            row += 1
    return board

def board_to_csv(board, csv):
    with open(csv, 'w') as f:
        for row in board:
            f.write(','.join(map(str, row)))

if __name__ == '__main__':
    board = '''
        7 6 0 0 5 0 0 1 3
        0 2 0 8 7 0 0 0 0
        8 0 0 0 0 0 0 0 0
        1 0 0 0 0 0 9 2 0
        2 0 0 0 6 0 0 0 4
        0 7 5 0 0 0 0 0 1
        0 0 0 0 0 0 0 0 2
        0 0 0 0 2 3 0 4 0
        4 9 0 0 8 0 0 3 6
    '''
    board = np.array(map(int, board.split()), dtype='intc')
    board.resize((9, 9))
    print SudokuSolver().solve(board)
