'''
Created on 2012-05-21

@author: mavc
'''
import sys
sys.path.append('..')

import time
import os.path
import numpy as np

from sudoku import sudokusolve

def main():
    par = os.path.pardir
    problem_folder = os.path.join(os.path.dirname(__file__), par,
                                  'problems')

    problems = ['solved', 'easy', 'hard', 'easterMonster', 'evil',
                'goldenNugget', 'starBurstLeo', 'tarek071223170000-052',
                'minimum1', 'minimum50', 'nearWorstCase']

    # Time how long it takes to solve each problem.
    solver = sudokusolve.SudokuSolver(11)
    for problem in problems:
        pfile = os.path.join(problem_folder, problem + '.sud')
        sfile = os.path.join(problem_folder, problem + 'Solution.sud')

        board, solution = map(sudokusolve.board_from_csv, (pfile, sfile))

        t = time.clock()
        solved = solver.solve(board)
        t = time.clock() - t

        if np.all(solved == solution):
            print '{} SOLVED in {:0.1f} seconds'.format(problem, t)
        else:
            print '{} FAILED'.format(problem)

if __name__ == '__main__':
    main()
